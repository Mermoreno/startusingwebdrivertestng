package com.itnove.trainings.junit.startUsingWebDriverLocal;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class MyFirstAutomatedTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://www.sport.es");
        driver.navigate().refresh();
        driver.navigate().to("http://www.mundodeportivo.es");
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();
        driver.navigate().to("http://www.garfield.com");
    }
}
