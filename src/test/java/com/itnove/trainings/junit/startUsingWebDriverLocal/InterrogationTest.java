package com.itnove.trainings.junit.startUsingWebDriverLocal;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class InterrogationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://www.google.com");
        assertTrue(driver.getTitle().equals("Google"));
        assertTrue(driver.getCurrentUrl().contains("www.google."));
        assertTrue(driver.getPageSource().contains("google"));
    }
}
