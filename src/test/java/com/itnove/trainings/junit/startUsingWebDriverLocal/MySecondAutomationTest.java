package com.itnove.trainings.junit.startUsingWebDriverLocal;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class MySecondAutomationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://www.sport.es");
        assertTrue(driver.getTitle().contains("SPORT"));
        assertTrue(driver.getPageSource().contains("Valverde"));
        assertTrue(driver.getCurrentUrl().contains("sport.es"));

        driver.navigate().to("http://www.mundodeportivo.com");
        assertTrue(driver.getTitle().contains("Mundo Deportivo"));
        assertTrue(driver.getCurrentUrl().contains("mundodeportivo.com"));
        assertTrue(driver.getPageSource().contains("Neymar"));


    }
}
