package com.itnove.trainings.junit.startUsingWebDriverLocal.pages.searchPage;

import com.itnove.trainings.junit.startUsingWebDriverLocal.BaseTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class HoverTest20171113 extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //1
        driver.get("http://opencart.votarem.lu/");
        //2
        Actions hover = new Actions(driver);
        WebElement desktop = driver.findElement(By.id("Desktops"));
        hover.moveToElement(desktop).build().perform();
        Thread.sleep(5000);
        //2
        WebElement pc = driver.findElement(By.xpath(".//*[@id='menu']/div[2]/ul/li[1]/div/div/ul/li[1]/a"));
        hover.moveToElement(pc).build().perform();
        Thread.sleep(5000);
        //3
        WebElement mac = driver.findElement(By.xpath(".//*[@id='menu']/div[2]/ul/li[1]/div/div/ul/li[2]/a"));
        hover.moveToElement(mac).build().perform();
        Thread.sleep(5000);
        //4
        WebElement alldesktop = driver.findElement(By.xpath(".//*[@id='menu']/div[2]/ul/li[1]/div/a"));
        hover.moveToElement(alldesktop).build().perform();
        Thread.sleep(5000);
        //5
        alldesktop.click();
        Thread.sleep(5000);
        /**
        Thread.sleep(5000);
        WebElement registerAccount=driver.findElement(By.id("account-register"));
        assertTrue(registerAccount.isDisplayed());
         **/
    }




}
