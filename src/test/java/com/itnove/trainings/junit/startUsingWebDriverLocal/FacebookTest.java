package com.itnove.trainings.junit.startUsingWebDriverLocal;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class FacebookTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //Anar a la pag
        driver.navigate().to("http://www.facebook.com");
        //Fer click a la capsa d'email
        WebElement textboxEmail = driver.findElement(By.id("email"));
        textboxEmail.click();
        Thread.sleep(3000);
        //escriure l'email
        textboxEmail.sendKeys("charochacha@hotmail.com");
        Thread.sleep(3000);
        //fer click a la capsa de pw
        WebElement textboxPassword = driver.findElement(By.id("pass"));
        textboxPassword.click();
        Thread.sleep(3000);
        //escriure el password
        textboxPassword.sendKeys("tarari");
        Thread.sleep(3000);
        //clicar al boto de login
        WebElement Botologin = driver.findElement(By.id("loginbutton"));
        Botologin.click();
        Thread.sleep(3000);
        WebElement muro = driver.findElement(By.id("stream_pagelet"));
        assertTrue(muro.isDisplayed());


    }
}
