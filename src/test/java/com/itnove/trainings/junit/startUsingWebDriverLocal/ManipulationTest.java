package com.itnove.trainings.junit.startUsingWebDriverLocal;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class ManipulationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //1
        driver.navigate().to("http://www.duckduckgo.com");
        //2
        WebElement textboxCerca = driver.findElement(By.id("search_form_input_homepage"));
        textboxCerca.click();
        //textboxCerca.submit();
        Thread.sleep(3000);
        //3
        textboxCerca.sendKeys("ibiza");
        Thread.sleep(3000);
        //4
        WebElement lupa = driver.findElement(By.id("search_button_homepage"));
        lupa.click();
        Thread.sleep(3000);
        //5.1
        assertTrue(driver.getTitle().contains("ibiza"));
        //5.2
        WebElement linksCerca = driver.findElement(By.id("links"));
        assertTrue(linksCerca.isDisplayed());
        WebElement linkCercaClick = driver.findElement(By.id("r1-2"));
        linkCercaClick.click();
        Thread.sleep(3000);
        assertTrue(!driver.getCurrentUrl().contains("duckduckgo"));






    }
}
